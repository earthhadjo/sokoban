const  map = [  
    "  WWWWW ",  
    "WWW   W ",  
    "WOSB  W ",  
    "WWW BOW ",  
    "WOWWB W ",  
    "W W O WW",  
    "WB XBBOW",  
    "W   O  W",  
    "WWWWWWWW"  
  ];
  let rows, cells, cellArray, cell, row;
  let gameboard=document.getElementById("gameboard");
  let playerRow =2;
  let playerColumn =2;
  
  function createMaze(){
      for( rows=0; rows< map.length; rows++){
          row= document.createElement('div');
          row.classList.add("row");
          cellArray= map[rows].split("")
          gameboard.appendChild(row);
          for (cells=0; cells<cellArray.length; cells++){
          cell= document.createElement("div");
          cell.classList.add('cell');
          cell.innerHTML = cellArray[cells];
          row.appendChild(cell);
  
         defineCell()
          }
      }
  }
  createMaze()
  
  function defineCell(){
      if (cell.innerHTML === "W"){
          cell.classList.add("wall");
          cell.innerHTML= ""
      }
      else if (cell.innerHTML === "S"){
          cell.id = "start";
          cell.innerHTML = ""
      }
      else if (cell.innerHTML === "F"){
          cell.id = "finish"
          cell.innerHTML = ""
      }
      else{
          cell.classList.add("floor");
      }    
      }
  
      function addPlayer(){
          player = document.createElement("img");
          player.src = "src/blackfist.gif";
          player.classList.add("player");
          document.getElementById("start").appendChild(player)
  
      }
  addPlayer()
  
      let playerTop = 0;
      let playerLeft = 0;
      
      document.addEventListener('keydown', logKey);
      function logKey(e) {
          if (e.keyCode===38){
           if (map[playerRow - 1][playerColumn] !== 'W') {   
          player.style.top = (playerTop -= 20)+ "px";
          playerRow -= 1;
           }    
      }
      else if (e.keyCode===37){
          if (map[playerRow][playerColumn - 1] !== 'W') {   
              player.style.left = (playerLeft -= 20)+ "px";
              playerColumn -= 1;
               } 
      }
      else if (e.keyCode===40){
          if (map[playerRow + 1][playerColumn] !== 'W') {   
              player.style.top = (playerTop += 20)+ "px";
              playerRow += 1;
               } 
      }    
      else if (e.keyCode===39){
          if (map[playerRow ][playerColumn + 1] !== 'W') {   
              player.style.left = (playerLeft += 20)+ "px";
              playerColumn += 1;
               }    
      }    
      
      }
  
      